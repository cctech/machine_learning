import csv
import json

import pandas as pd
import pymysql as sql
from sklearn.cluster import KMeans

NUM_CLUSTERS = 10
connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

data = pd.read_sql("SELECT * from pass_customerid_assessments;", connection)

continuous_feats = ['age', 'work_experience', 'net_worth', 'fsw_score', 'crs_score',
                    'qsw_score', 'id', 'payments']  # , 'web_contact_count']
cont_data = data.loc[:, continuous_feats]

json_lang_1 = data.loc[:, ['lang1']]
json_feats = ["eng_read", "eng_write", "eng_listen", "eng_speak", "fr_read", "fr_write", "fr_listen", "fr_speak"]
json_data = pd.DataFrame(columns=json_feats)
continuous_feats.extend(json_feats)
json_lang_2 = data.loc[:, ['lang2']]
json_lang_1 = json_lang_1.values.tolist()
json_lang_2 = json_lang_2.values.tolist()

lst = []
for lang_1, lang_2 in zip(json_lang_1, json_lang_2):
    eng = json.loads(lang_1[0])["skills"]
    fr = json.loads(lang_2[0])["skills"]
    lst.append({
        "eng_read": eng["reading"],
        "eng_write": eng["writing"],
        "eng_listen": eng["listening"],
        "eng_speak": eng["speaking"],
        "fr_read": fr["reading"],
        "fr_write": fr["writing"],
        "fr_listen": fr["listening"],
        "fr_speak": fr["speaking"]
    })
json_data = json_data.append(lst, ignore_index=True)

cont_data = pd.concat([cont_data, json_data], axis=1)
for feature in continuous_feats:
    if feature != 'id' and feature != 'payments':
        cont_data[feature] = (cont_data[feature].astype(float) - cont_data[feature].astype(float).mean()) / cont_data[
            feature].astype(float).std()
cont_data.fillna(0, inplace=True)
# cont_data = cont_data.values

km = KMeans(n_clusters=NUM_CLUSTERS).fit(cont_data)
cluster_map = pd.DataFrame()
cluster_map['data_index'] = cont_data.index.values
cluster_map['cluster'] = km.labels_

with open("/root/machine_learning/files/Clusters.csv", mode='w') as forward_clustered:
    writer = csv.writer(forward_clustered, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['Cluster', 'id', 'payments'])
    for i in range(NUM_CLUSTERS):
        for j in cluster_map[cluster_map.cluster == i]['data_index']:
            writer.writerow([i + 1, cont_data["id"][j], cont_data["payments"][j]])
