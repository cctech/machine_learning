import pandas as pd
import pymysql as sql

connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

noc = pd.read_sql(
    "SELECT noc, avg(crs_score), count(crs_score) from pass_customerid_assessments where noc is not null group by noc order by avg(crs_score) DESC LIMIT 100;",
    connection)
noc.dropna(inplace=True)
noc.to_csv('/root/machine_learning/files/crs_noc.csv')

# data.plot(kind='bar', x='residence_country', y='count(*)')
# plt.show()
# plt.savefig('/root/machine_learning/files/country_bar.png')
