import json
import pickle

import pymysql as sql
import torch
from torch.utils.data import DataLoader

from model_dataset import LeadDataset
from pandas_preprocess import pre_process
from util import *


def use(email):
    model = torch.load("/root/machine_learning/files/main_model")
    with open("/root/machine_learning/files/label_encoder.pickle", 'rb') as handle:
        label_encoder = pickle.load(handle)
    with open("/root/machine_learning/files/norm_stats.pickle", 'rb') as handle:
        norm_stats = pickle.load(handle)

    # Use config file to load sql info
    with open("./config.json", 'rb') as handle:
        config = json.load(handle)
    mysql_config = config['mysql']
    connection = sql.connect(host=mysql_config['host'],
                             user=mysql_config['user'],
                             password=mysql_config['password'],
                             db=mysql_config['db'])

    data = pd.read_sql(
        f"SELECT * from assessments WHERE email='{email}' and result = 'PASS' and rule = 'general_evaluator' order by created_at desc limit 30;",
        connection)

    print('Assessment(s) Loaded!')
    email = pd.read_sql(f"SELECT * from num_emails where email='{email}';", connection)
    email_2018 = pd.read_sql(f"SELECT email, count(*) num_emails_2018 from item_contacts where email='{email}';",
                             connection)
    print('Emails Loaded!')
    payments = pd.read_sql(
        f"SELECT email, count(*) payments from transactions where status = 'approved' and email='{email}';", connection)
    data['email'] = data['email'].str.lower()
    email['email'] = email['email'].str.lower()
    email_2018['email'] = email_2018['email'].str.lower()
    payments['email'] = payments['email'].str.lower()

    data = data.merge(email, on='email', how='left')
    data = data.merge(payments, on='email', how='left')
    data = data.merge(email_2018, on='email', how='left')
    data[['num_emails_2019', 'payments', 'num_emails_2018']] = data[
        ['num_emails_2019', 'payments', 'num_emails_2018']].fillna(value=0)
    data['num_emails_2018'] = np.sqrt(data['num_emails_2018']).astype(int)
    data['num_emails'] = data['num_emails_2019'] + data['num_emails_2018']
    if data.empty:
        return ('No passing assessments matching this email!')

    # Extract features
    id = data['id'].to_numpy()
    crs = data['crs_score'].to_numpy()

    # preprocess data
    faux_labels = np.multiply((data['payments'] != 0).values, 1)
    data = pre_process(data, 'use', label_encoder, norm_stats)[0]
    data = np.multiply(data.astype(float), 1)
    faux_labels = np.multiply(faux_labels, 1)

    data_loader = DataLoader(LeadDataset(data, faux_labels), batch_size=data.shape[0])

    for i, vbatch in enumerate(data_loader):
        # Use the model
        feats = vbatch[0]
        predictions = model(feats.float())

    # Save the predictions
    pred = np.concatenate((
        id.reshape(-1, 1), predictions.cpu().detach().numpy().reshape(-1, 1), crs.reshape(-1, 1)), axis=1).tolist()
    for i in range(len(pred)):
        pred[i] = {'id': pred[i][0], 'score': pred[i][1], 'crs': pred[i][2]}

    return pred
