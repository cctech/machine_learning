from flask import jsonify
from flask import request

from app import app
from db_config import mysql
from use_by_email import use


@app.route('/assessment/add', methods=['POST'])
def add_assessment():
    try:
        connection_created = False
        everything_loaded = False
        _json = request.get_json()
        _id = _json['id']
        _token = _json['token']
        _parent_assessment_id = _json['parent_assessment_id']
        _first_name = _json['first_name']
        _email = _json['email']
        _email_sent = _json['email_sent']
        _phone = _json['phone']
        _age = _json['age']
        _marital_status = _json['marital_status']
        _origin_country = _json['origin_country']
        _work_experience = _json['work_experience']
        _data = _json['data']
        _rule = _json['rule']
        _result = _json['result']
        _fsw_score = _json['fsw_score']
        _crs_score = _json['crs_score']
        _qsw_score = _json['qsw_score']
        _device = _json['device']
        _referral = _json['referral']
        _ip = _json['ip']
        _created_at = _json['created_at']
        _updated_at = _json['updated_at']
        _noc = _json['noc']
        _level = _json['level']
        _last_name = _json['last_name']
        _track_kt = _json['track_kt']
        _track_cat = _json['track_cat']
        _track_site = _json['track_site']
        _is_mobile = _json['is_mobile']
        _residence_country = _json['residence_country']
        _response_sent = _json['response_sent']
        _education = _json['education']
        everything_loaded = True

        if request.method == 'POST':
            # save edits
            sql = "INSERT INTO assessments(id, token, parent_assessment_id, first_name, email, email_sent, phone, age, " \
                  "marital_status, origin_country, work_experience, data, rule, result, fsw_score, crs_score, " \
                  "qsw_score, device, referral, ip, created_at, updated_at, noc, level, last_name, track_kt, " \
                  "track_cat, track_site, is_mobile, residence_country, response_sent, education) VALUES(" \
                  "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
                  " %s, %s, %s, %s, %s, %s, %s, %s)"
            data = (_id, _token, _parent_assessment_id, _first_name, _email, _email_sent, _phone, _age, _marital_status,
                    _origin_country, _work_experience, _data, _rule, _result, _fsw_score, _crs_score, _qsw_score,
                    _device, _referral, _ip, _created_at, _updated_at, _noc, _level, _last_name, _track_kt, _track_cat,
                    _track_site, _is_mobile, _residence_country, _response_sent, _education,)
            connection_created = True
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('User added successfully!')
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        if not everything_loaded:
            return f'{e} not loaded properly'
        print(e)
    finally:
        if connection_created:
            cursor.close()
            conn.close()


@app.route('/transaction/add', methods=['POST'])
def add_transaction():
    try:
        connection_created = False
        everything_loaded = False
        _json = request.get_json()
        _id = _json['id']
        _assessment_id = _json['assessment_id']
        _address = _json['address']
        _first_name = _json['first_name']
        _email = _json['email']
        _city = _json['city']
        _phone = _json['phone']
        _country = _json['country']
        _state_or_province = _json['state_or_province']
        _zip_or_postal_code = _json['zip_or_postal_code']
        _card_owner = _json['card_owner']
        _card_last_digits = _json['card_last_digits']
        _card_type = _json['card_type']
        _card_expiration = _json['card_expiration']
        _processor = _json['processor']
        _processor_transaction_id = _json['processor_transaction_id']
        _processor_response = _json['processor_response']
        _device = _json['device']
        _referral = _json['referral']
        _ip = _json['ip']
        _created_at = _json['created_at']
        _updated_at = _json['updated_at']
        _type = _json['type']
        _status = _json['status']
        _last_name = _json['last_name']
        _track_kt = _json['track_kt']
        _track_cat = _json['track_cat']
        _track_site = _json['track_site']
        _is_mobile = _json['is_mobile']
        _description = _json['description']
        _currency = _json['currency']
        _category = _json['category']
        _amount = _json['amount']
        _tax = _json['tax']
        _net = _json['net']

        everything_loaded = True

        if request.method == 'POST':
            # save edits
            sql = "INSERT INTO transactions(id, assessment_id, address, first_name, email, city, phone, country, " \
                  "state_or_province, zip_or_postal_code, card_owner, card_last_digits, card_type, card_expiration, " \
                  "processor, processor_transaction_id, processor_response, device, referral, ip, created_at, " \
                  "updated_at, type, status, last_name, track_kt, track_cat, track_site, is_mobile, amount, tax, net, currency, category, description)" \
                  " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
                  " %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            data = (_id, _assessment_id, _address, _first_name, _email, _city, _phone, _country, _state_or_province,
                    _zip_or_postal_code, _card_owner, _card_last_digits, _card_type, _card_expiration, _processor,
                    _processor_transaction_id, _processor_response, _device, _referral, _ip, _created_at, _updated_at,
                    _type, _status, _last_name, _track_kt, _track_cat, _track_site, _is_mobile, _amount, _tax, _net, _currency, _category, _description)
            connection_created = True
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('User added successfully!')
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        if not everything_loaded:
            return f'{e} not loaded properly'
        print(e)
    finally:
        if connection_created:
            cursor.close()
            conn.close()


@app.route('/user/', methods=['GET'])
def user():
    email = request.args.get('email', '')
    try:
        pred = use(email)
        resp = jsonify(pred)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)


@app.route('/email_count/add', methods=['POST'])
def update_user():
    try:
        connection_created = False
        _json = request.get_json()
        _email = _json['email'].lower()
        if _email and request.method == 'POST':
            # save edits
            sql = "INSERT IGNORE INTO num_emails(email) VALUES(%s)"
            sql2 = " UPDATE num_emails SET num_emails_2019 = num_emails_2019 + 1 WHERE email=%s"
            data = (_email,)
            conn = mysql.connect()
            connection_created = True
            cursor = conn.cursor()
            cursor.execute(sql, data)
            cursor.execute(sql2, data)
            conn.commit()
            resp = jsonify('Email count updated successfully!')
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        if connection_created:
            cursor.close()
            conn.close()


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
