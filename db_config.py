import json

from flaskext.mysql import MySQL

from app import app

mysql = MySQL()
with open("./config.json", 'rb') as handle:
    config = json.load(handle)
mysql_config = config['mysql']

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = mysql_config['user']
app.config['MYSQL_DATABASE_PASSWORD'] = mysql_config['password']
app.config['MYSQL_DATABASE_DB'] = mysql_config['db']
app.config['MYSQL_DATABASE_HOST'] = mysql_config['host']
mysql.init_app(app)
