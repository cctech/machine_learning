import pickle

import ahocorasick

A = ahocorasick.Automaton()
for idx, key in enumerate(['how long',
                           'processing time',
                           'how much',
                           'cost',
                           'installment',
                           'fee',
                           'pay',
                           'ielts',
                           'celpip',
                           ' tef',
                           'language test',
                           'clb',
                           'inadmissible',
                           'crime',
                           'job',
                           'felony',
                           'misdemeanour',
                           'crs',
                           'score',
                           'draw',
                           'education',
                           'degree',
                           'work',
                           'selected']):
    A.add_word(key, (idx, key))

A.make_automaton()

with open("/root/machine_learning/files/Automaton.pickle", 'wb') as handle:
    pickle.dump(A, handle, protocol=pickle.HIGHEST_PROTOCOL)
