import numpy as np
import pandas as pd
import pymysql as sql

connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

data = pd.read_sql("SELECT * from assessments", connection)
fsw = data['fsw_score'].to_numpy()
crs = data['crs_score'].to_numpy()

pred = np.concatenate((
    fsw.reshape(-1, 1), crs.reshape(-1, 1)),
    axis=1)
np.savetxt("/root/machine_learning/files/fsw_crs.csv", pred, delimiter=',')
