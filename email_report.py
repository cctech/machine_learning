import gzip
import pickle
from html.parser import HTMLParser

import pandas as pd
import pymysql as sql


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

with open("/root/machine_learning/files/Automaton.pickle", 'rb') as handle:
    A = pickle.load(handle)

body = pd.read_sql("SELECT body from exchange_items limit 1;", connection)

metadict = {"Processing Time": 0, "Fees": 0, "Language Test": 0, "Criminality": 0, "CRS": 0, 'Job': 0, 'Education': 0}
for i in range(len(body.index)):
    dict = {"Processing Time": 0, "Fees": 0, "Language Test": 0, "Criminality": 0, "CRS": 0, 'Job': 0, 'Education': 0}
    haystack = str(gzip.decompress(body['body'].iloc[i]))
    s = MLStripper()
    s.feed(haystack)
    haystack = s.get_data()
    haystack = haystack.split("<dcohen@canadavisa.com>", 1)[0]
    haystack = haystack.split("wrote:", 1)[0]
    haystack = haystack.lower()
    haystack = haystack.replace("\\r", "")
    haystack = haystack.replace("\\n", "")
    print(haystack)
    for end_index, (insert_order, original_value) in A.iter(haystack):
        start_index = end_index - len(original_value) + 1
        # print((start_index, end_index, (insert_order, original_value)))
        if original_value == 'how long' or original_value == 'processing time':
            dict['Processing Time'] = 1
        elif original_value == 'how much' or original_value == 'cost' or original_value == 'installment' or original_value == 'fee' or original_value == 'pay':
            dict['Fees'] = 1
        elif original_value == 'ielts' or original_value == 'celpip' or original_value == ' tef' or original_value == 'language test' or original_value == 'clb':
            dict['Language Test'] = 1
        elif original_value == 'inadmissable' or original_value == 'crime' or original_value == 'felony' or original_value == 'misdemeanour':
            dict['Criminality'] = 1
        elif original_value == 'crs' or original_value == 'score' or original_value == 'draw' or original_value == 'selected':
            dict['CRS'] = 1
        elif original_value == 'job' or original_value == 'work':
            dict['Job'] = 1
        elif original_value == 'education' or original_value == 'degree':
            dict['Education'] = 1

        assert haystack[start_index:start_index + len(original_value)] == original_value
    print([k for k, v in dict.items() if v == 1])
    for k, v in dict.items():
        if v == 1:
            metadict[k] += 1

with open("/root/machine_learning/files/metadict.pickle", 'wb') as handle:
    pickle.dump(metadict, handle, protocol=pickle.HIGHEST_PROTOCOL)
