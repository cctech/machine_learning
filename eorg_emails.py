import pandas as pd
import pymysql as sql

connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

data = pd.read_sql("SELECT * from eorg_aug17_aug19", connection)
email = data['email']
field4 = data[data["field4"].str.count('@') == 1]["field4"]

email = email.append(field4)

email.to_csv(r"/root/machine_learning/files/email_list.csv", header=None, index=None, sep=',', mode='a')
