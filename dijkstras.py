import sys


def shortest_path(graph, current_state, goal_state, visited=None, distances=None, predecessors=None):
    # if we haven't started yet, set the distance to 0 of current_state to current_state
    if visited is None:
        visited = []
        distances = {current_state: 0}
        predecessors = {}

    # if we've gotten to the end return
    if current_state == goal_state:
        path = []
        while goal_state is not None:
            path.append(goal_state)
            goal_state = predecessors.get(goal_state, None)
        return distances[current_state], path[::-1]

    for neighbour in graph[current_state]:
        if neighbour not in visited:
            neighbour_dist = distances.get(neighbour, sys.maxsize)
            check_dist = distances[current_state] + graph[current_state][neighbour]
            if check_dist < neighbour_dist:
                distances[neighbour] = check_dist
                predecessors[neighbour] = current_state

    # Mark current_state as visited
    visited.append(current_state)

    # look for the closest unvisited node
    unvisited = dict((k, distances.get(k, sys.maxsize)) for k in graph if k not in visited)
    closest_node = min(unvisited, key=unvisited.get)

    # rerun shortest path with next node
    return shortest_path(graph, closest_node, goal_state, visited, distances, predecessors)


if __name__ == '__main__':
    graph = {
        ('bachelor', 'low language'): {
            ('masters', 'good language'): 17,
            ('masters', 'low language'): 10,
            ('bachelor', 'good language'): 10},
        ('masters', 'low language'): {
            ('masters', 'good language'): 10,
        },
        ('bachelor', 'good language'): {
            ('masters', 'good language'): 10,
        },
        ('masters', 'good language'): {
        }
    }
    print(shortest_path(graph, ('bachelor', 'low language'), ('masters', 'low language')))
    print(shortest_path(graph, ('bachelor', 'low language'), ('masters', 'good language')))
