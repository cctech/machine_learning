import argparse
import json
from datetime import datetime
from time import time

import pymysql as sql
import torch
import torch.nn as nn
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader

from model_dataset import MultiLayerPerceptron, LeadDataset
from pandas_preprocess import pre_process
from sampler import ImbalancedDatasetSampler
from util import *


# =================================== LOAD DATA AND MODEL =========================================== #

def load_data(batch_size, training_data, training_label, valid_data, valid_label):
    ######

    # Stores the features and the labels in LeadDataset (defined in model_dataset.py)
    train_set = LeadDataset(training_data, training_label)
    val_set = LeadDataset(valid_data, valid_label)
    ######

    # Use torch DataLoader which separates the data into batches so that we can access them later by batch.
    # ImbalancedDatasetSampler is imported from sampler.py and it ensures that there are equal amounts of both classes
    # by oversampling the people who pay and undersampling the people who don't in a weighted draw.
    train_loader = DataLoader(train_set, batch_size=batch_size,
                              sampler=ImbalancedDatasetSampler(train_set, training_label))
    val_loader = DataLoader(val_set, batch_size=valid_data.shape[0])

    return train_loader, val_loader


def load_model(lr, input_size, Ns):
    ######

    # Initializes the model stored in model_dataset.py with the size of the data and with the number of samples
    model = MultiLayerPerceptron(input_size, Ns)
    # Defines the loss function as binary cross entropy loss as this is the best loss function for binary classification
    loss_fnc = torch.nn.BCELoss()
    # Initializes the Adam optimizer with the learning rate that is input
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    ######

    return model, loss_fnc, optimizer


def evaluate(model, val_loader):
    total_corr = 0

    for i, vbatch in enumerate(val_loader):
        # In the validation set there should only be one batch but in case we're using this function for other purposes
        # we do it this way
        feats, label = vbatch

        # Find the prediction by running the model
        prediction = model(feats.float())

        # Calculate the number of correct predictions
        corr = (prediction > 0.5).squeeze().long() == label

        total_corr += int(corr.sum())

        # Calculate the number of Type 2 and Type 1 errors, note this total will only work if there is one batch
        type1 = int(((prediction > 0.5).squeeze().long() > label).sum())
        type2 = int(((prediction > 0.5).squeeze().long() < label).sum())

    # Return accuracy as well as number of type 1 and type 2 errors
    return float(total_corr) / len(val_loader.dataset), type1, type2


def main():
    # Load a config.json that contains usernames and passwords
    with open("./config.json", 'rb') as handle:
        config = json.load(handle)
    mysql_config = config['mysql']

    # Set up hyperparameters using argsparse in case want to change them in line
    parser = argparse.ArgumentParser()

    # Batch size is the number of samples it learns on before it updates the weight
    parser.add_argument('--batch_size', type=int, default=32)

    # Learning rate is how much it updates the weights by, larger numbers have more volatility, however smaller numbers
    # are slower
    parser.add_argument('--lr', type=float, default=1e-4)

    # Epochs are the amount of times it trains on the entire dataset
    parser.add_argument('--epochs', type=int, default=5)

    # eval_every is after how many steps does it print an update on the training and evaluate the validation set
    parser.add_argument('--eval_every', type=int, default=500)

    args = parser.parse_args()

    seed = 5

    # =================================== LOAD DATASET =========================================== #

    ######

    # connect to the database
    connection = sql.connect(host=mysql_config['host'],
                             user=mysql_config['user'],
                             password=mysql_config['password'],
                             db=mysql_config['db'])

    # calculate the date exactly 2 months ago (presumed salecycle)
    salecycle = monthdelta(datetime.now(), -2)

    # Load the data in from different tables in the mysql database
    data = pd.read_sql(
        f"SELECT * from assessments WHERE created_at > '2018-08-22 0:0:0' and created_at < '{salecycle}' and email is not null and result = 'PASS' and rule = 'general_evaluator';",
        connection)
    print('Assessments Loaded!')
    email = pd.read_sql("SELECT * from num_emails where email is not null;", connection)
    email_2018 = pd.read_sql(
        "SELECT email, count(*) num_emails_2018 from item_contacts where email is not null group by email;", connection)
    print('Emails Loaded!')
    payments = pd.read_sql(
        "SELECT email, count(*) payments from transactions where status = 'approved' and email is not null group by email;",
        connection)
    print('Payments Loaded!')
    connection.close()
    data['email'] = data['email'].str.lower()
    email['email'] = email['email'].str.lower()
    email_2018['email'] = email_2018['email'].str.lower()
    payments['email'] = payments['email'].str.lower()

    # Join all the data queries on email
    data = data.merge(email, on='email', how='left')
    data = data.merge(payments, on='email', how='left')
    data = data.merge(email_2018, on='email', how='left')

    # Fill NaN values with 0 for those columns
    data[['num_emails_2019', 'payments', 'num_emails_2018']] = data[
        ['num_emails_2019', 'payments', 'num_emails_2018']].fillna(value=0)

    # Square root the number of emails from 2018 (Abid's database of emails wasn't comparable to Jonathan's so needed
    # some way of scaling it non-linearly, square root increased accuracy by quite a bit, this can use some improvement
    # to a more logical reason)
    data['num_emails_2018'] = np.sqrt(data['num_emails_2018']).astype(int)

    # Sum email's from 2019 (Jonthan's dump) and 2018 (Abid's dump)
    data['num_emails'] = data['num_emails_2019'] + data['num_emails_2018']

    # Only train on assessments with 1 or more emails to increase accuracy and as a way of creating more balance in the
    # data set.
    data = data[data['num_emails'] >= 1]
    ######

    # Pre-process the dta with code found in pandas_preprocess.py in 'training' mode
    paid_data = (data['payments'] != 0).values
    paid_data = np.multiply(paid_data, 1)

    train_data, val_data, train_label, val_label = train_test_split(
        data, paid_data, test_size=0.2, shuffle=False, random_state=seed)

    train_data, label_encoder, norm_stats = pre_process(train_data, 'training')
    val_data = pre_process(val_data, 'Validation', label_encoder, norm_stats)[0]

    # multiply the data by 1 to convert booleans to numbers and convert data to float in case there are any strings
    train_data = np.multiply(train_data.astype(float), 1)
    val_data = np.multiply(val_data.astype(float), 1)

    # =================================== MAKE THE TRAIN AND VAL SPLIT =========================================== #
    # make use of the train_test_split method to randomly divide our dataset into two portions
    # control the relative sizes of the two splits using the test_size parameter

    ######

    # sm = SMOTE(random_state=12, ratio=1.0)
    # train_data, train_label = sm.fit_sample(train_data, train_label)

    print(train_label.sum())
    print(val_label.sum())

    ######

    ######

    # Call the load_data function defined above
    train_loader, val_loader = load_data(args.batch_size, train_data, train_label, val_data, val_label)
    # Call the load_model function defined as above
    model, loss_fnc, optimizer = load_model(args.lr, train_data.shape[1], train_data.shape[0])

    # Set up the initial variables and lists
    t = 0
    train_acc = []
    sec = []
    val_acc = []
    val_type2_acc = []
    step = []
    train_cor_eval = 0

    # Find the start time (useful if timing how long it takes)
    start = time()

    # Start the training loop with the number of epochs provided
    for epoch in range(args.epochs):
        # Initialize local training variables
        accum_loss = 0
        tot_corr = 0
        type1 = 0
        type2 = 0

        for i, batch in enumerate(train_loader):

            # For each batch in the train_loader defined in load_model perform the following steps

            feats, label = batch

            # Clear the gradients from the previous batch
            optimizer.zero_grad()

            # print(feats.data.numpy()[0])

            # Compute the prediction
            predictions = model(feats.float())

            # print(predictions.squeeze(), label.float())

            # Calculate the loss using loss_fnc define in load model
            batch_loss = loss_fnc(input=predictions.squeeze(), target=label.float())

            # Keep track of total loss for the epoch
            accum_loss += batch_loss

            # Use backpropogation using the batch_loss to calculate the gradients
            batch_loss.backward()

            # Use the optimizer to proceed one step (depends on learning rate) along the negative gradient (lower loss)
            optimizer.step()

            # Calculate the number of correct predictions
            corr = (predictions > 0.5).squeeze().long() == label

            # Calculate other statistics
            tot_corr += int(corr.sum())
            type1 += int(((predictions > 0.5).squeeze().long() > label).sum())
            type2 += int(((predictions > 0.5).squeeze().long() < label).sum())
            train_cor_eval += int(corr.sum())

            if (t + 1) % args.eval_every == 0:
                # If we're in one of the eval_every steps, we evaluate the gradient

                # Calculate time and append it
                sec.append(time() - start)
                # Evaluate the validation set
                valid_acc, val_type1, val_type2 = evaluate(model, val_loader)
                # Find current training accuracy
                train_acc_eval = float(train_cor_eval) / (args.batch_size * args.eval_every)

                # Append to lists for easy graphing
                train_acc.append(train_acc_eval)
                val_acc.append(valid_acc)
                val_type2_acc.append((val_label.sum() - val_type2) / val_label.sum())
                step.append(t + 1)

                # Print an update
                print("Epoch: {}, Step {} | Loss: {}| Validation acc: {}".format(epoch + 1, t + 1, accum_loss / 100,
                                                                                 valid_acc))
                print("Val Type 2 Acc: {}".format((val_label.sum() - val_type2) / val_label.sum()))
                accum_loss = 0
                train_cor_eval = 0

            t += 1

    # Print final updates
    print("Train acc:{}".format(float(tot_corr) / len(train_loader.dataset)))
    print("Training type 1 error: {}, Training type 2 error: {}".format(type1, type2))
    torch.save(model, '/root/machine_learning/files/main_model')

    # Graph figures showing how accuracy and loss progress over time or steps

    # plt.figure()
    # plt.title("Accuracy over time")
    # # plt.plot(np.array(step), np.array(train_acc), label="Train")
    # # plt.plot(np.array(step), np.array(val_acc), label="Validation")
    # plt.plot(np.array(sec), sig.savgol_filter(np.array(train_acc), polyorder=1, window_length=3), label="Train")
    # plt.plot(np.array(sec), sig.savgol_filter(np.array(val_acc), polyorder=1, window_length=3), label="Validation")
    # plt.plot(np.array(sec), sig.savgol_filter(np.array(val_type2_acc), polyorder=1, window_length=3),
    #          label="Validation Type 2 Accuracy")
    # plt.xlabel("Time")
    # plt.ylabel("Accuracy")
    # plt.legend(loc='best')
    # plt.show()

    # Run model_test from model_testing.py
    # model_test(model, label_encoder, norm_stats)

    # Run use from use_model.py
    # use(model, label_encoder, norm_stats)


if __name__ == "__main__":
    main()
