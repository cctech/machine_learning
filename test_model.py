import pandas as pd
import torch

if __name__ == "__main__":
    model = torch.load("/root/machine_learning/files/main_model")
    df = pd.DataFrame(columns=['education', 'marital_status', 'level', 'has_job_offer', 'paid', 'residence_country',
                               'newsletter_signup', 'noc', 'payments', 'age', 'work_experience', 'net_worth',
                               'fsw_score', 'crs_score', 'qsw_score', '2019_num_emails', 'lang1', 'lang2'], index=['x'])
