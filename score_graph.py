import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pymysql as sql

connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

data = pd.read_sql("SELECT qsw_score, payments from pass_customerid_assessments;", connection)
data.dropna()
data.sort_values('qsw_score', inplace=True)

crs_score = data['qsw_score'].values
cumulative = []
count = 0
payments = 0
for x in data['payments']:
    if x != 0:
        payments += 1
    count += 1
    cumulative.append(payments / count)

plt.figure()
plt.title('Cumulative payments vs. QSW')
plt.plot(np.array(crs_score), np.array(cumulative))
plt.show()
plt.savefig('/root/machine_learning/files/qsw_graph.png')
