import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pymysql as sql

connection = sql.connect(host='localhost',
                         user='ccmachinelearning',
                         password='pass4ml',
                         db='canadavisa_evaluator_prod')

data = pd.read_sql("SELECT residence_country, count(*) from pass_customerid_assessments group by residence_country;",
                   connection)
data.dropna(inplace=True)

# data['residence_country'] = [0 if x == 'US' else 1 if x == 'QA' or x == 'SA' or x == 'AE' or x == 'EG'
# else 2 if x == 'NG' else 3 if x == 'GB' or x == 'IE' else 4 if x == 'NO' or x == 'DE' or x == 'ES'
# else 5 for x in data['residence_country']]

N = 6
us = data.loc[data['residence_country'] == 0]
gulf = data.loc[data['residence_country'] == 1]
ng = data.loc[data['residence_country'] == 2]
uk = data.loc[data['residence_country'] == 3]
eu = data.loc[data['residence_country'] == 4]
oth = data.loc[data['residence_country'] == 5]

totMeans = (us['crs_score'].count(), gulf['crs_score'].count(), ng['crs_score'].count(), uk['crs_score'].count(),
            eu['crs_score'].count(), oth['crs_score'].count())
# totStd = (us['crs_score'].std(), gulf['crs_score'].std(), ng['crs_score'].std(), uk['crs_score'].std(),
#           eu['crs_score'].std(), oth['crs_score'].std())

us_paid = us.loc[us['payments'] != 0]
gulf_paid = gulf.loc[gulf['payments'] != 0]
ng_paid = ng.loc[ng['payments'] != 0]
uk_paid = uk.loc[uk['payments'] != 0]
eu_paid = eu.loc[eu['payments'] != 0]
oth_paid = oth.loc[oth['payments'] != 0]

fig, ax = plt.subplots()

ind = np.arange(N)  # the x locations for the groups
width = 0.35  # the width of the bars
p1 = ax.bar(ind, totMeans, width, bottom=0)  # , yerr=totStd)

paidMeans = (us_paid['crs_score'].count(), gulf_paid['crs_score'].count(), ng_paid['crs_score'].count(),
             uk_paid['crs_score'].count(), eu_paid['crs_score'].count(), oth_paid['crs_score'].count())
# paidStd = (us_paid['crs_score'].std(), gulf_paid['crs_score'].std(), ng_paid['crs_score'].std(),
#            uk_paid['crs_score'].std(), eu_paid['crs_score'].std(), oth_paid['crs_score'].std())
p2 = ax.bar(ind + width, paidMeans, width, bottom=0)  # , yerr=paidStd)

ax.set_title('Count of assessments passed by country and payment')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('US', 'Gulf', 'Nigeria', 'UK', 'Europe', 'Other'))

ax.legend((p1[0], p2[0]), ('Total', 'Paid'))
ax.autoscale_view()

plt.show()
plt.savefig('/root/machine_learning/files/country_bar.png')
