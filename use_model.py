import json
import pickle
from datetime import datetime

import pymysql as sql
import torch
from torch.utils.data import DataLoader

from model_dataset import LeadDataset
from pandas_preprocess import pre_process
from util import *


def use(model, label_encoder, norm_stats):
    # Use config file to load sql info
    with open("./config.json", 'rb') as handle:
        config = json.load(handle)
    mysql_config = config['mysql']
    connection = sql.connect(host=mysql_config['host'],
                             user=mysql_config['user'],
                             password=mysql_config['password'],
                             db=mysql_config['db'])
    salecycle = monthdelta(datetime.now(), -2)

    # Load assessments and data from within the salecycle
    data = pd.read_sql(
        f"SELECT * from assessments WHERE created_at > '{salecycle}' and email is not null and result = 'PASS' and rule = 'general_evaluator';",
        connection)
    print('Assessments Loaded!')
    email = pd.read_sql("SELECT * from num_emails where email is not null;", connection)
    email_2018 = pd.read_sql(
        "SELECT email, count(*) num_emails_2018 from item_contacts where email is not null group by email;", connection)
    print('Emails Loaded!')
    payments = pd.read_sql(
        "SELECT email, count(*) payments from transactions where status = 'approved' and email is not null group by email;",
        connection)
    print('Payments Loaded!')
    data['email'] = data['email'].str.lower()
    email['email'] = email['email'].str.lower()
    email_2018['email'] = email_2018['email'].str.lower()
    payments['email'] = payments['email'].str.lower()
    data = data.merge(email, on='email', how='left')
    data = data.merge(payments, on='email', how='left')
    data = data.merge(email_2018, on='email', how='left')
    data[['num_emails_2019', 'payments', 'num_emails_2018']] = data[
        ['num_emails_2019', 'payments', 'num_emails_2018']].fillna(value=0)
    data['num_emails_2018'] = np.sqrt(data['num_emails_2018']).astype(int)
    data['num_emails'] = data['num_emails_2019'] + data['num_emails_2018']
    data = data[data['num_emails'] >= 1]

    # Extract features
    email = data['email'].to_numpy()
    crs = data['crs_score'].to_numpy()
    age = data['age'].to_numpy()
    level = data['level'].to_numpy()
    num_emails = data['num_emails'].to_numpy()
    residence_country = data['residence_country'].to_numpy()

    # preprocess data
    faux_labels = np.multiply((data['payments'] != 0).values, 1)
    data = pre_process(data, 'use', label_encoder, norm_stats)[0]
    data = np.multiply(data.astype(float), 1)
    faux_labels = np.multiply(faux_labels, 1)

    data_loader = DataLoader(LeadDataset(data, faux_labels), batch_size=data.shape[0])

    for i, vbatch in enumerate(data_loader):
        # Use the model
        feats = vbatch[0]
        predictions = model(feats.float())

    # Save the predictions
    pred = np.concatenate((
        email.reshape(-1, 1), predictions.cpu().detach().numpy().reshape(-1, 1), crs.reshape(-1, 1), age.reshape(-1, 1),
        level.reshape(-1, 1), residence_country.reshape(-1, 1), num_emails.reshape(-1, 1)), axis=1)
    np.savetxt("/root/machine_learning/files/predictions_main.csv", pred, delimiter=',', fmt='%s')


if __name__ == "__main__":
    model = torch.load("/root/machine_learning/files/main_model")
    with open("/root/machine_learning/files/label_encoder.pickle", 'rb') as handle:
        label_encoder = pickle.load(handle)
    with open("/root/machine_learning/files/norm_stats.pickle", 'rb') as handle:
        norm_stats = pickle.load(handle)
    use(model, label_encoder, norm_stats)
