import torch
import torch.nn as nn


class LeadDataset(torch.utils.data.Dataset):

    def __init__(self, features, label):
        ######

        self.features = features
        self.label = label

        ######

    def __len__(self):
        return len(self.features)

    def __getitem__(self, index):
        ######

        return self.features[index], self.label[index]

        ######


class MultiLayerPerceptron(nn.Module):

    def __init__(self, input_size, Ns):
        super(MultiLayerPerceptron, self).__init__()

        ###### Meh? Model takes forever to run though
        # a = 5
        # Nh = int(Ns/(a*(input_size+1)))
        # self.fc1 = nn.Linear(input_size, Nh)
        # self.fc2 = nn.Linear(Nh, int(Nh/2))
        # self.fc3 = nn.Linear(int(Nh/2), 1)
        # # self.bn1 = nn.BatchNorm1d(Nh)
        # # self.bn2 = nn.BatchNorm1d(int(Nh/2))

        #### Quicker Net

        # First layer brings it to size 64
        self.fc1 = nn.Linear(input_size, 64)
        # Initialize the weights with Xavier Initialization
        nn.init.xavier_uniform_(self.fc1.weight)
        # Initialize the bias with zeros
        nn.init.zeros_(self.fc1.bias)
        # Second layer brings it from 64 to 32 nodes
        self.fc2 = nn.Linear(64, 32)
        nn.init.xavier_uniform_(self.fc2.weight)
        nn.init.zeros_(self.fc2.bias)
        # Lastly it brings it from 32 to 1 which is the result
        self.fc3 = nn.Linear(32, 1)
        nn.init.xavier_uniform_(self.fc3.weight)
        nn.init.zeros_(self.fc3.bias)
        # self.bn1 = nn.BatchNorm1d(64)
        # self.bn2 = nn.BatchNorm1d(32)

        ######

    def forward(self, features):
        ######
        # RRelu is a randomized leaky relu function so that adds non-linearity so that its not just linear regression
        output = self.fc1(features)
        output = torch.rrelu(output)
        # output = self.bn1(output)
        output = self.fc2(output)
        output = torch.rrelu(output)
        # output = self.bn2(output)
        output = self.fc3(output)
        # After bringing it to 1, run a sigmoid on it to have the value between 0 to 1
        output = torch.sigmoid(output)

        return output
        ######
