import pickle

import matplotlib.pyplot as plt

with open("/Users/jmosseri/CV_graphs/metadict.pickle", 'rb') as handle:
    metadict = pickle.load(handle)

plt.bar(range(len(metadict)), list(metadict.values()), align='center')
plt.xticks(range(len(metadict)), list(metadict.keys()))
plt.show()
print(metadict)
