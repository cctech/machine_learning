import json
import pickle
import sys
from collections import defaultdict

import pymysql as sql
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

from util import *


# Language Jsons taken into account, BOC found, and label_encoder saved as a pickled dict


def pre_process(data, mode="None", label_encoder=None, norm_stats=None):
    if norm_stats is None:
        norm_stats = defaultdict(dict)

    # =================================== DATA VISUALIZATION =========================================== #

    # print(data.shape)
    # print(data.columns)
    # verbose_print(data.head())
    data.reset_index(inplace=True)
    print(data["payments"].value_counts())

    # =================================== DATA CLEANING =========================================== #

    # let's first count how many missing entries there are for each feature
    # col_names = data.columns
    # num_rows = data.shape[0]

    # Paid is a 1 if payments is not 0
    data.loc[:, 'paid'] = data['payments'] != 0

    # Fill NaN with -1 so we can still do calculations if its continuous and if not we can have its own category
    data.fillna('-1', inplace=True)

    # Seperate noc into boc
    data.loc[:, 'boc'] = [x[1] if x[0] == '0' else x[0] for x in data['noc']]

    # Split residence country into groups
    data.loc[:, 'residence_country'] = [0 if x == 'US' else 1 if x == 'QA' or x == 'SA' or x == 'AE' or x == 'EG'
    else 2 if x == 'NG' else 3 if x == 'GB' or x == 'IE' else 4 if x == 'NO' or x == 'DE' or x == 'ES'
    else 5 for x in data['residence_country']]

    # for feature in col_names:
    #     ######
    #    print(data[feature].isin(["?"]).sum())

    # print(data.shape)

    ######

    # =================================== BALANCE DATASET =========================================== #

    ######
    # Imbalanced Dataset sampler has been used

    ### Undersampling by removing most of the data to balance

    # data = pd.concat([data[data["paid"] == 0].sample(n=data["paid"].value_counts(
    #     ascending=True).iloc[0], random_state=seed), data[data["paid"] == 1].sample(
    #     n=data["paid"].value_counts(ascending=True).iloc[0], random_state=seed)])

    ######

    # =================================== DATA STATISTICS =========================================== #

    # our dataset contains both continuous and categorical features. In order to understand our continuous features better,
    # we can compute the distribution statistics (e.g. mean, variance) of the features using the .describe() method

    ######

    # If visualizing data then print some stuff
    if mode == "Visualization":
        print("\nFULL:\n")
        verbose_print(data.describe())
        print("\nNO PAY:\n")
        verbose_print(data[data['paid'] == 0].describe())
        print("\nPAY:\n")
        verbose_print(data[data['paid'] == 1].describe())

    # likewise, let's try to understand the distribution of values for discrete features. More specifically, we can check
    # each possible value of a categorical feature and how often it occurs
    categorical_feats = ['education', 'level', 'boc', 'residence_country']
    binary_feats = ['has_job_offer', 'newsletter_signup', 'marital_status']

    if mode == "Visualization":
        # Print data by categorical feat
        categorical_feats.append('response_sent')
        for feature in categorical_feats:
            #
            print("\nFULL:\n")
            print(data[feature].value_counts(normalize=True))
            print("\nNO PAY:\n")
            print(data[data['paid'] == 0][feature].value_counts(normalize=True))
            print("\nPAY:\n")
            print(data[data['paid'] == 1][feature].value_counts(normalize=True))
            # pie_chart(data,feature)  # want to see all of them so put this here
            # binary_bar_chart(data,feature)  # want to see all of them so put this here
        # Exit if mode is Visualization after printing
        sys.exit()
        ######

    # =================================== DATA PREPROCESSING =========================================== #

    # we need to represent our categorical features as 1-hot encodings
    # we begin by converting the string values into integers using the LabelEncoder class
    # next we convert the integer representations into 1-hot encodings using the OneHotEncoder class
    # we don't want to convert 'income' into 1-hot so let's extract this field first
    # we also need to preprocess the continuous features by normalizing against the feature mean and standard deviation
    # don't forget to stitch continuous and cat features together

    # NORMALIZE CONTINUOUS FEATURES
    ######

    # Extract continuous feats
    continuous_feats = ['age', 'work_experience', 'fsw_score', 'crs_score', 'qsw_score', 'num_emails', 'marital_status']
    cont_data = data.loc[:, continuous_feats]
    # Since marital status is binary if we replace with 0 and 1 it can act as a continuous feat
    cont_data.replace(to_replace={'single': 0, 'married': 1}, inplace=True)

    # List of feats to extract from json
    json_feats = ["eng_read", "eng_write", "eng_listen", "eng_speak", "fr_read", "fr_write", "fr_listen", "fr_speak",
                  'has_job_offer', 'newsletter_signup', 'net_worth']
    json_data = pd.DataFrame(columns=json_feats)
    continuous_feats.extend(json_feats)
    # json_lang_1 = data.loc[:, ['lang1']]
    # json_lang_2 = data.loc[:, ['lang2']]
    # json_lang_1 = json_lang_1.values.tolist()
    # json_lang_2 = json_lang_2.values.tolist()
    json_full = data.loc[:, ['data']].values.tolist()
    lst = []
    for json_row in json_full:
        # Extract feats from json
        json_load = json.loads(json_row[0])
        eng = json_load["language"]["primary"]["skills"]
        fr = json_load["language"]["secondary"]["skills"]
        offer = json_load['canadian_job_offer']
        news = json_load["news_letter"]
        lst.append({
            "eng_read": eng["reading"],
            "eng_write": eng["writing"],
            "eng_listen": eng["listening"],
            "eng_speak": eng["speaking"],
            "fr_read": fr["reading"],
            "fr_write": fr["writing"],
            "fr_listen": fr["listening"],
            "fr_speak": fr["speaking"],
            'has_job_offer': False if 'has_offer' not in offer else offer['has_offer'],
            'newsletter_signup': False if 'immigration_newsletter' not in news else news['immigration_newsletter'],
            'net_worth': -1 if json_load['personal_net_worth'] == [] else json_load['personal_net_worth']["net_worth"]
        })
    json_data = json_data.append(lst, ignore_index=True)
    # Replace NaN in Json with -1
    json_data.fillna('-1', inplace=True)

    # Concatenate continuous and json data
    cont_data = pd.concat([cont_data, json_data], axis=1)

    # Find continuous feats that aren't binary so we can normalize them
    continuous_feats = [item for item in continuous_feats if item not in binary_feats]
    for feature in continuous_feats:
        # Normalize the continuous feat but avoid division by 0
        if mode == 'training':
            mean = cont_data[feature].astype(float).mean()
            std = cont_data[feature].astype(float).std() if cont_data[feature].astype(float).std() != 0 else 1
            norm_stats[feature] = {'mean': mean, 'std': std}

        cont_data[feature] = (cont_data[feature].astype(float) - norm_stats[feature]['mean']) / norm_stats[feature][
            'std']
    cont_data = cont_data.values

    ######

    # ENCODE CATEGORICAL FEATURES AND SAVE Norm_stats
    if mode == 'training':
        with open("/root/machine_learning/files/norm_stats.pickle", 'wb') as handle:
            pickle.dump(norm_stats, handle, protocol=pickle.HIGHEST_PROTOCOL)
        # If mode is training create a new label encoder for each categorical feat
        label_encoder = defaultdict(LabelEncoder)
        cat_data = data.loc[:, categorical_feats]
        for feature in categorical_feats:
            cat_data[feature] = label_encoder[feature].fit_transform(cat_data[feature])
        with open("/root/machine_learning/files/label_encoder.pickle", 'wb') as handle:
            pickle.dump(label_encoder, handle, protocol=pickle.HIGHEST_PROTOCOL)
            # Save it
    else:
        cat_data = data.loc[:, categorical_feats]
        for feature in categorical_feats:
            cat_data[feature] = label_encoder[feature].transform(cat_data[feature])

    ######

    oneh_feats = ['education', 'level', 'boc', 'residence_country']
    oneh_encoder = {'education': OneHotEncoder(categories=[list(range(len(label_encoder['education'].classes_)))]),
                    'level': OneHotEncoder(categories=[list(range(len(label_encoder['level'].classes_)))]),
                    'boc': OneHotEncoder(categories=[list(range(len(label_encoder['boc'].classes_)))]),
                    'residence_country':
                        OneHotEncoder(categories=[list(range(len(label_encoder['residence_country'].classes_)))])}
    ######
    oneh_data = cat_data
    for feature in oneh_feats:
        oneh_data = np.concatenate(
            (oneh_data, oneh_encoder[feature].fit_transform(cat_data[feature].values.reshape(-1, 1)).toarray()), axis=1)
    cat_data = oneh_data[:, len(categorical_feats) - 1:]

    ######
    # .toarray() converts the DataFrame to a numpy array

    data = np.concatenate((cont_data, cat_data), axis=1)

    return data, label_encoder, norm_stats


if __name__ == "__main__":
    connection = sql.connect(host='localhost',
                             user='ccmachinelearning',
                             password='pass4ml',
                             db='canadavisa_evaluator_prod')

    data = pd.read_sql(
        "SELECT * from pass_customerid_assessments WHERE 2019_num_emails >= 0 and created_at > '2019-01-01 0:0:0' and crs_score >= 440 and residence_country = 'US';",
        connection)
    data, label_encoder = pre_process(data, "Visualization")
